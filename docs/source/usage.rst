Usage
=====

Installation
------------

To use Lumache, first install it using pip:

.. code-block:: console

   (.venv) $ pip install lumache

Testing stuff  
-------------
.. autofunction:: file.function
.. autofunction:: file.np

